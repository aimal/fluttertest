import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Appbar Button Shift',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double scrollOffset = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Appbar Button Shift'),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.blue,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.blue,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.blue,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.blue,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.blue,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
            ],
          ),
          Positioned(
            top: scrollOffset,
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              color: Colors.red,
              child: TextButton(
                onPressed: () {},
                child: Text('Button'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void scrollListener() {
    setState(() {
      var scrollController;
      scrollOffset = scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    super.initState();
    var scrollController = ScrollController()..addListener(scrollListener);
  }

  @override
  void dispose() {
    var scrollController;
    scrollController.dispose();
    super.dispose();
  }
}
