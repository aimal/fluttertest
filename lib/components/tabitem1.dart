import 'package:flutter/material.dart';

class TabItem1 extends StatelessWidget {
  bool isMobile = false;
  TabItem1(this.isMobile);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildHeader(),
        SizedBox(height: 50),
        _buildStepRow("1.", "Erstellen deines Lebenslaufs", "profiledata.png",
            false, true),
        SizedBox(height: 50),
        _buildStepRow(
            "2.", "Erstellen deines Lebenslaufs", "drawtask.png", true, false),
        SizedBox(height: 50),
        _buildStepRow("3.", "Mit nur einem Klick bewerben", "personalfile.png",
            false, false),
      ],
    );
  }

  Widget _buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Column(
          children: [
            Text(
              "Drei einfache Schritte  ",
              style: TextStyle(color: Color(0xff4A5568), fontSize: 21),
            ),
            Text(
              "zu deinem neuen Job",
              style: TextStyle(color: Color(0xff4A5568), fontSize: 21),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildStepRow(String number, String title, String imageName,
      bool flipped, bool mobileFlipped) {
    return isMobile
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              (!isMobile && flipped) || (isMobile && mobileFlipped)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 0, right: 0),
                      child:
                          Image(image: AssetImage("assets/images/$imageName")),
                    )
                  : Text(""),
              Padding(
                padding: const EdgeInsets.only(bottom: 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      number,
                      style: TextStyle(color: Color(0xff718096), fontSize: 100),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: Text(
                        title,
                        style:
                            TextStyle(color: Color(0xff718096), fontSize: 15),
                      ),
                    ),
                  ],
                ),
              ),
              (!flipped && !isMobile) || (isMobile && !mobileFlipped)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 0, left: 50),
                      child:
                          Image(image: AssetImage("assets/images/$imageName")),
                    )
                  : Text(""),
            ],
          )
        : Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              flipped
                  ? Padding(
                      padding: const EdgeInsets.only(top: 0, right: 50),
                      child:
                          Image(image: AssetImage("assets/images/$imageName")),
                    )
                  : Text(""),
              Padding(
                padding: const EdgeInsets.only(bottom: 0),
                child: Text(
                  number,
                  style: TextStyle(color: Color(0xff718096), fontSize: 130),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: Text(
                  title,
                  style: TextStyle(color: Color(0xff718096), fontSize: 30),
                ),
              ),
              !flipped
                  ? Padding(
                      padding: const EdgeInsets.only(top: 0, left: 50),
                      child:
                          Image(image: AssetImage("assets/images/$imageName")),
                    )
                  : Text(""),
            ],
          );
  }
}
