import 'package:flutter/material.dart';

class MainSection extends StatelessWidget {
  bool isMobile = false;
  MainSection(this.isMobile);

  @override
  Widget build(BuildContext context) {
    return !isMobile
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Deine Job",
                      style: TextStyle(color: Color(0xff2D3748), fontSize: 42),
                    ),
                    Text(
                      "website",
                      style: TextStyle(color: Color(0xff2D3748), fontSize: 42),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15, right: 50),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Color(0xff319795),
                                  Color(0xff3182CE)
                                ])),
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 2, bottom: 2, left: 10, right: 10),
                          child: TextButton(
                              onPressed: null,
                              child: Text(
                                'Kostenlos Registrieren',
                                style: TextStyle(color: Color(0xffE6FFFA)),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              CircleAvatar(
                radius: 96,
                backgroundImage: AssetImage("assets/images/agreement.png"),
                // child:  AssetImage("assets/images/agreement.png")
              ),
            ],
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Deine Job",
                      style: TextStyle(color: Color(0xff2D3748), fontSize: 42),
                    ),
                    Text(
                      "website",
                      style: TextStyle(color: Color(0xff2D3748), fontSize: 42),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Image(image: AssetImage("assets/images/agreement.png")),
              )
            ],
          );
  }
}
