import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webviewapp/components/mainsection.dart';
import 'package:webviewapp/components/tabitem1.dart';
import 'package:webviewapp/components/tabitem2.dart';
import 'package:webviewapp/components/tabitem3.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String _currenttext = "Arbeitnehmer";
  double boxSize = 1200;
  late ScrollController _scrollController;
  bool _isAppBarExpanded = false;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      setState(() {
        _isAppBarExpanded = _scrollController.offset > 280;
      });
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double currentSize = MediaQuery.sizeOf(context).width;
    bool isMobile = currentSize < 768;

    // String groupvalue = 0;
    return Scaffold(
      appBar: AppBar(
          toolbarHeight: 60,
          excludeHeaderSemantics: true,
          backgroundColor: Colors.transparent,
          shadowColor: Color(0xffeeeeee),
          elevation: 10.0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                color: Color(0xffFFFFFF),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                _isAppBarExpanded && !isMobile
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Jetzt Klicken",
                          style: TextStyle(color: Color(0xff4A5568)),
                        ),
                      )
                    : Text(""),
                _isAppBarExpanded && !isMobile
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(
                                  color: Color(0xffCBD5E0), width: 1)),
                          child: TextButton(
                            onPressed: () {},
                            child: const Text(
                              "Kostenlos Registrieren",
                              style: TextStyle(
                                  color: Color(0xff319795), fontSize: 14),
                            ),
                          ),
                        ),
                      )
                    : Text(""),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextButton(
                    onPressed: () {},
                    child: const Text(
                      "Login",
                      style: TextStyle(color: Color(0xff319795), fontSize: 14),
                    ),
                  ),
                ),
              ]),
            ),
          ),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20)),
          )),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(color: Color(0xffEBF4FF)),
              child: Padding(
                  padding: isMobile
                      ? const EdgeInsets.symmetric(vertical: 20)
                      : const EdgeInsets.symmetric(vertical: 96),
                  child: MainSection(isMobile)),
            ),
            Column(
              children: [
                const SizedBox(height: 40),
                CupertinoSegmentedControl<String>(
                  borderColor: Color(0xffCBD5E0),

                  groupValue: _currenttext, // Set the initial selected segment
                  children: {
                    "Arbeitnehmer": Container(
                      color: _currenttext == "Arbeitnehmer"
                          ? Color(0xff81E6D9)
                          : Color(0xffffffff),
                      width: double.infinity,
                      // Ensure full width
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Arbeitnehmer",
                          style: TextStyle(
                              color: _currenttext == "Arbeitnehmer"
                                  ? const Color(0xffffffff)
                                  : const Color(0xff319795)),
                        ),
                      ),
                    ),
                    "Arbeitgeber": Container(
                      color: _currenttext == "Arbeitgeber"
                          ? const Color(0xff81E6D9)
                          : const Color(0xffffffff),
                      width: double.infinity,
                      // Ensure full width
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Arbeitgeber",
                          style: TextStyle(
                              color: _currenttext == "Arbeitgeber"
                                  ? const Color(0xffffffff)
                                  : const Color(0xff319795)),
                        ),
                      ),
                    ),
                    "Temporärbüro": Container(
                      color: _currenttext == "Temporärbüro"
                          ? const Color(0xff81E6D9)
                          : const Color(0xffffffff),
                      width: double.infinity,
                      // Ensure full width
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Temporärbüro",
                          style: TextStyle(
                              color: _currenttext == "Temporärbüro"
                                  ? const Color(0xffffffff)
                                  : const Color(0xff319795)),
                        ),
                      ),
                    ),
                  },
                  onValueChanged: (String value) {
                    setState(() {
                      _currenttext = value;
                    });
                  },
                ),
                const SizedBox(height: 50),
                _currenttext == "Arbeitnehmer"
                    ? TabItem1(isMobile)
                    : _currenttext == "Arbeitgeber"
                        ? TabItem2(isMobile)
                        : _currenttext == "Temporärbüro"
                            ? TabItem3(isMobile)
                            : Text("Not Found")
              ],
            ),
            CupertinoSegmentedControl<String>(
              groupValue:
                  _currenttext, // State variable tracking selected segment
              selectedColor: Colors.amber,
              unselectedColor: Colors.black,
              borderColor: Colors.orange,
              pressedColor: Colors.brown.shade200,
              children: {
                "Arbeitnehmer": Container(
                  color: _currenttext == "Arbeitnehmer"
                      ? const Color(0xff81E6D9)
                      : const Color(0xffFFFFFF),
                ),
                "Arbeitgeber": Container(
                  color: _currenttext == "Arbeitgeber"
                      ? const Color(0xff81E6D9)
                      : const Color(0xffFFFFFF),
                ),
                "Temporärbüro": Container(
                  color: _currenttext == "Temporärbüro"
                      ? const Color(0xff81E6D9)
                      : const Color(0xffFFFFFF),
                ),
              },
              onValueChanged: (String value) {
                setState(() {
                  _currenttext = value;
                });
              },
            )
          ],
        ),
      ),
      bottomNavigationBar: !isMobile
          ? Text("")
          : Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffcccccc),
                    spreadRadius: 1,
                    blurRadius: 10,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [Color(0xff319795), Color(0xff3182CE)])),
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 2, bottom: 2, left: 10, right: 10),
                    child: TextButton(
                        onPressed: null,
                        child: Text(
                          'Kostenlos Registrieren',
                          style: TextStyle(color: Color(0xffE6FFFA)),
                        )),
                  ),
                ),
              ),
            ),
    );
  }
}
